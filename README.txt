Database used: Derby

To compile: mvn compile

Run Unit Tests: mvn test

Run server: mvn jetty:run

URLs:
List Products: http://localhost:8080/web/api/products
Get Product: http://localhost:8080/web/api/products/{id}

The basic CRUD is implemented, for the Product only.

Unfortunally I had no time to implement the other features.




