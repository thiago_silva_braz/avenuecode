package avenuecode;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.plugins.server.tjws.TJWSEmbeddedJaxrsServer;
import org.junit.BeforeClass;
import org.junit.Test;

public class ProductServiceTest {

	@BeforeClass
	public static void setUp() {
		TJWSEmbeddedJaxrsServer tjws = new TJWSEmbeddedJaxrsServer();
		tjws.setPort(8081);
		tjws.start();
		tjws.getDeployment().getRegistry().addPerRequestResource(ProductService.class, "web/api");
	}

	@Test
	public void testPost() {
		Product p = post("Test Product");
		assertNotNull(p);
		assertEquals("Test Product", p.getName());

	}

	public Product post(String productName) {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://localhost:8081/web/api/products");
		Builder request = target.request();
		Product productP = new Product();
		productP.setName(productName);
		Response response = request.post(Entity.entity(productP, MediaType.APPLICATION_XML));
		assertEquals(200, response.getStatus());
		return response.readEntity(Product.class);
	}

	@Test
	public void testPut() {
		Product product = post("Test Product 1");
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://localhost:8081/web/api/products/" + product.getId());
		Builder request = target.request();
		product.setName("Test Product 2");
		Response response = request.put(Entity.entity(product, MediaType.APPLICATION_XML));
		assertEquals(200, response.getStatus());
		Product productR = response.readEntity(Product.class);
		assertNotNull(productR);
		assertEquals("Test Product 2", productR.getName());
	}

	@Test
	public void testGet() {
		Product product1 = post("Test Product 1");
		Product product2 = post("Test Product 2");
		
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://localhost:8081/web/api/products/" + product1.getId());
		Builder request = target.request();
		Response response = request.get();
		assertEquals(200, response.getStatus());
		Product productR = response.readEntity(Product.class);
		assertNotNull(productR);
		assertEquals(product1.getName(), productR.getName());

		client = ClientBuilder.newClient();
		target = client.target("http://localhost:8081/web/api/products/" + product2.getId());
		request = target.request();
		response = request.get();
		assertEquals(200, response.getStatus());
		productR = response.readEntity(Product.class);
		assertNotNull(productR);
		assertEquals(product2.getName(), productR.getName());
	}

	@Test
	public void testDelete() {
		Product product = post("Test Product 1");
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://localhost:8081/web/api/products/" + product.getId());
		Builder request = target.request();
		Response response = request.delete();
		assertEquals(200, response.getStatus());

		client = ClientBuilder.newClient();
		target = client.target("http://localhost:8081/web/api/products/" + product.getId());
		request = target.request();
		response = request.get();
		assertEquals(204, response.getStatus());
	}

}
