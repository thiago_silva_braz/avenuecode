package avenuecode;

import javax.persistence.NoResultException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/products")
@Produces("application/xml")
@Consumes("application/xml")
public class ProductService {
	@GET
	@Path("/")
	public Products list() {
		return new Products(new ProductDAO().list());
	}

	@GET
	@Path("/{id}")
	public Product get(@PathParam("id") int id) {
		try {
			return new ProductDAO().get(id);
		} catch (NoResultException e) {
			return null;
		}
	}

	@POST
	@Path("/")
	public Product post(Product product) {
		ProductDAO productDAO = new ProductDAO();
		productDAO.persist(product);
		return product;
	}

	@PUT
	@Path("/{id}")
	public Product put(@PathParam("id") int id, Product productData) {
		ProductDAO productDAO = new ProductDAO();
		return productDAO.update(id, productData);
	}

	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") int id) {
		ProductDAO productDAO = new ProductDAO();
		try {
			Product product = new ProductDAO().get(id);
			productDAO.delete(product);
			return Response.ok().build();
		} catch (NoResultException e) {
			return Response.noContent().build();
		}

	}
}