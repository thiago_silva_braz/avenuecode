package avenuecode;

import java.util.HashSet;
import java.util.Set;

public class Application extends javax.ws.rs.core.Application {
	private Set<Object> singletons = new HashSet<Object>();

	public Application() {
		singletons.add(new ProductService());
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
