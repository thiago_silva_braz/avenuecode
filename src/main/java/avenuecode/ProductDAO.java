package avenuecode;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ProductDAO {

	static final EntityManagerFactory factory = Persistence.createEntityManagerFactory("default");
	private EntityManager entityManager;

	public ProductDAO() {
		entityManager = factory.createEntityManager();
	}

	public List<Product> list() {
		return entityManager.createQuery("SELECT p FROM Product p", Product.class).getResultList();
	}

	public void persist(Product product) {
		entityManager.getTransaction().begin();
		try {
			entityManager.persist(product);
			entityManager.flush();
			entityManager.getTransaction().commit();
		} catch (RuntimeException e) {
			entityManager.getTransaction().rollback();
			throw e;
		}
	}

	public void delete(Product product) {
		entityManager.getTransaction().begin();
		try {
			entityManager.remove(get(product.getId()));
			entityManager.getTransaction().commit();
		} catch (RuntimeException e) {
			entityManager.getTransaction().rollback();
			throw e;
		}
	}

	public Product get(int id) {
		return entityManager.find(Product.class, id);
	}

	public Product update(int id, Product productData) {
		entityManager.getTransaction().begin();
		try {
			Product product = get(id);
			product.setName(productData.getName());
			if (productData.getParentProduct() != null && productData.getParentProduct().getId() > 0) {
				Product parent = get(productData.getParentProduct().getId());
				product.setParentProduct(parent);
			}
			entityManager.getTransaction().commit();
			return product;
		} catch (RuntimeException e) {
			entityManager.getTransaction().rollback();
			throw e;
		}
	}
}
