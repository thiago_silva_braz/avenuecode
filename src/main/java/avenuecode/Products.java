package avenuecode;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Products {

	@XmlElement
	private List<Product> list;

	Products() {

	}

	public Products(List<Product> list) {
		this.list = list;
	}

	public List<Product> getProduct() {
		return list;
	}

}
