package avenuecode;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppContextListener implements ServletContextListener {

	public void contextInitialized(ServletContextEvent servletContextEvent) {
		ProductDAO productDAO = new ProductDAO();
		Product product = new Product();
		product.setName("Product 1");
		Image image = new Image();
		image.setDescription("Image 1");
		product.addImage(image);
		productDAO.persist(product);

		Product product2 = new Product();
		product2.setName("Product 2");
		product2.setParentProduct(product);
		productDAO.persist(product2);
	}

	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		
	}

}